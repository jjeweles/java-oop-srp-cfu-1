package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class AuthorTest {

    Author author;

    @BeforeEach
    public void setUp() {
        author = new Author("Eric", "Evans");
    }

    @Test
    public void authorFormattedNameReturnsCorrectly() {
        String expected = "Evans, Eric";
        assertEquals(expected, author.getFormattedName());
    }

    @Test
    public void getAuthorFirstNameAndLastNameReturnCorrectValues() {
        assertEquals("Eric", author.getFirstName());
        assertEquals("Evans", author.getLastName());
    }

    @Test
    public void setAuthorFirstNameAndLastNameReturnCorrectValues() {
        author.setFirstName("John");
        author.setLastName("Smith");
        assertEquals("John", author.getFirstName());
        assertEquals("Smith", author.getLastName());
    }

    @Test
    public void instantiateAuthorWithNoArgs() {
        Author author = new Author();
        assertNull(author.getFirstName());
        assertNull(author.getLastName());
    }

}
