package com.galvanize;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BookTest {

    Author author;
    Publisher publisher;
    Book book;

    @BeforeEach
    public void setUp() {
        author = new Author("Eric", "Evans");
        publisher = new Publisher("Addison-Wesley", "New York");
        book = new Book("Domain-Driven Design", 2004, author, publisher);
    }

    @Test
    public void bookFormattedNameReturnsCorrectly() {

        String expected = "Domain-Driven Design (2004)\n" +
                "Written by Evans, Eric\n" +
                "Published by Addison-Wesley, New York";

        assertEquals(expected, book.getFormattedName());
    }

    @Test
    public void getBookYearAndGetBookTitleReturnCorrectValues() {
        assertEquals(2004, book.getYear());
        assertEquals("Domain-Driven Design", book.getTitle());
    }

    @Test
    public void setBookYearAndGetBookTitleReturnCorrectValues() {
        book.setYear(2005);
        book.setTitle("Domain-Driven Design: Tackling Complexity in the Heart of Software");
        assertEquals(2005, book.getYear());
        assertEquals("Domain-Driven Design: Tackling Complexity in the Heart of Software", book.getTitle());
    }

    @Test
    public void getBookAuthorAndPublisherReturnCorrectValues() {
        assertEquals(author, book.getAuthor());
        assertEquals(publisher, book.getPublisher());
    }

    @Test
    public void instantiateBookWithNoArgs() {
        Book book = new Book();
        assertNull(book.getTitle());
        assertEquals(0, book.getYear());
        assertNull(book.getAuthor());
        assertNull(book.getPublisher());
    }
}
