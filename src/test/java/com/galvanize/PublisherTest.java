package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class PublisherTest {

    Publisher publisher;

    @BeforeEach
    public void setUp() {
        publisher = new Publisher("Addison-Wesley", "New York");
    }

    @Test
    public void publisherFormattedNameReturnsCorrectly() {
        String expected = "Addison-Wesley, New York";
        assertEquals(expected, publisher.getFormattedName());
    }

    @Test
    public void getPublisherNameAndCityReturnCorrectValues() {
        assertEquals("Addison-Wesley", publisher.getName());
        assertEquals("New York", publisher.getCity());
    }

    @Test
    public void setPublisherNameAndCityReturnCorrectValues() {
        publisher.setName("Penguin");
        publisher.setCity("London");
        assertEquals("Penguin", publisher.getName());
        assertEquals("London", publisher.getCity());
    }

    @Test
    public void instantiatePublisherWithNoArgs() {
        Publisher publisher = new Publisher();
        assertNull(publisher.getName());
        assertNull(publisher.getCity());
    }

}
