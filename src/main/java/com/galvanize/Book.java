package com.galvanize;

public class Book {

    private String title;
    private int year;
    private Author author;
    private Publisher publisher;

    public Book() {
    }

    public Book(String bookTitle, int bookYear, Author author, Publisher publisher) {
        this.title = bookTitle;
        this.year = bookYear;
        this.author = author;
        this.publisher = publisher;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String bookName) {
        this.title = bookName;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public String getFormattedName() {
        return String.format(
                "%s (%s)\nWritten by %s\nPublished by %s",
                getTitle(),
                getYear(),
                this.author.getFormattedName(),
                this.publisher.getFormattedName()
        );
    }
}
